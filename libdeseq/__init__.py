__copyright__ = "Copyright (C) 2020 Blaise Li"
__licence__ = "GNU GPLv3"
__version__ = 0.2
from .libdeseq import (
    do_deseq2)
