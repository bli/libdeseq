# Copyright (C) 2020 Blaise Li
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import warnings


def formatwarning(message, category, filename, lineno, line):
    """Used to format warning messages."""
    return "%s:%s: %s: %s\n" % (filename, lineno, category.__name__, message)


warnings.formatwarning = formatwarning

import pandas as pd
import rpy2.robjects as ro
from rpy2.robjects import r, pandas2ri, Formula, StrVector
as_df = r("as.data.frame")
from rpy2.robjects.packages import importr
from rpy2.robjects.conversion import localconverter
deseq2 = importr("DESeq2")


def do_deseq2(cond_names, conditions, counts_data,
              formula=None, contrast=None, deseq2_args=None):
    """Runs a DESeq2 differential expression analysis."""
    if formula is None:
        formula = Formula("~ lib")
    if contrast is None:
        # FIXME: MUT and REF are not defined
        # Maybe just make (formula and) contrast mandatory
        contrast = StrVector(["lib", MUT, REF])
    if deseq2_args is None:
        deseq2_args = {"betaPrior" : True, "addMLE" : True, "independentFiltering" : True}
    col_data = pd.DataFrame(conditions).assign(
        cond_name=pd.Series(cond_names).values).set_index("cond_name")
    # In case we want contrasts between factor combinations
    if ("lib" in col_data.columns) and ("treat" in col_data.columns):
        col_data = col_data.assign(
            lib_treat = ["%s_%s" % (lib, treat) for (lib, treat) in zip(
                col_data["lib"], col_data["treat"])])
    col_data_rownames = list(col_data.index)
    counts_data_colnames = list(counts_data.columns)
    if col_data_rownames != counts_data_colnames:
        warnings.warn(
            "The lines in the sample description do not match "
            "the columns in the counts table.\n"
            "Expect failures while loading data in DESeq2.\n")
    # http://stackoverflow.com/a/31206596/1878788
    pandas2ri.activate()  # makes some conversions automatic
    ## debug
    py2rpy_ndarray_issue = """Conversion 'py2rpy' not defined for objects of type '<class 'numpy.ndarray'>'"""
    ##
    # r_counts_data = pandas2ri.py2ri(counts_data)
    # r_col_data = pandas2ri.py2ri(col_data)
    # r.DESeqDataSetFromMatrix(countData=r_counts_data, colData=r_col_data, design=Formula("~lib"))
    # dds = deseq2.DESeq(dds, betaPrior=deseq2_args["betaPrior"])
    # Decompose into the 3 steps to have more control on the options
    dds = deseq2.DESeqDataSetFromMatrix(
        countData=counts_data,
        colData=col_data,
        design=formula)
    # try:
    #     dds = deseq2.DESeqDataSetFromMatrix(
    #         countData=counts_data,
    #         colData=col_data,
    #         design=formula)
    # except RuntimeError as e:
    #     # TODO: remove this debugging thing, or use a unique path
    #     # and issue a warning that indicates the path to the debug file
    #     col_data.to_csv("/tmp/col_data_debug.txt", sep="\t")
    #     counts_data.to_csv("/tmp/counts_data_debug.txt", sep="\t")
    #     raise
    try:
        dds = deseq2.estimateSizeFactors_DESeqDataSet(dds, type="ratio")
    except RuntimeError as e:
        if sum(counts_data.prod(axis=1)) == 0:
            msg = "".join(["Error occurred in estimateSizeFactors:\n%s\n" % e,
                           "This is probably because every gene has at least one zero.\n",
                           "We will try to use the \"poscounts\" method instead."])
            warnings.warn(msg)
            try:
                dds = deseq2.estimateSizeFactors_DESeqDataSet(dds, type="poscounts")
            except RuntimeError as e:
                msg = "".join(["Error occurred in estimateSizeFactors:\n%s\n" % e,
                               "We give up."])
                warnings.warn(msg)
                raise
                #print(counts_data.dtypes)
                #print(counts_data.columns)
                #print(len(counts_data))
                #raise
        else:
            raise
    with localconverter(ro.default_converter + pandas2ri.converter):
        size_factors = deseq2.sizeFactors_DESeqDataSet(dds)
        # d2_sfs = deseq2.sizeFactors_DESeqDataSet(dds)
        # try:
        #     size_factors_rdf = as_df(d2_sfs)
        #     size_factors = ro.conversion.rpy2py(size_factors_rdf)
        # except NotImplementedError as e:
        #     if str(e) == py2rpy_ndarray_issue:
        #         warnings.warn(str(e))
        #         warnings.warn("d2_sfs:\n")
        #         warnings.warn(str(d2_sfs))
        #         warnings.warn("\n")
        #         size_factors = d2_sfs
        #     else:
        #         raise
        #try:
        #    size_factors = d2_sfs
        #except NotImplementedError as e:
        #    if str(e) == py2rpy_ndarray_issue:
        #        warnings.warn(str(e))
        #        warnings.warn("size_factors_rdf:\n")
        #        warnings.warn(str(size_factors_rdf))
        #        warnings.warn("\n")
        #    raise
    #for cond in cond_names:
    #    #s = size_factors.loc[cond][0]
    #    #(*_, s) = size_factors.loc[cond]
    #pd.DataFrame({cond : size_factors.loc[cond][0] for cond in COND_NAMES}, index=('size_factor',))
    try:
        dds = deseq2.estimateDispersions_DESeqDataSet(dds, fitType="parametric")
    except RuntimeError as e:
        msg = "".join(["Error occurred in estimateDispersions:\n%s\n" % e,
                    "We will try with fitType=\"local\"."])
        warnings.warn(msg)
        try:
            dds = deseq2.estimateDispersions_DESeqDataSet(dds, fitType="local")
        except RuntimeError as e:
            msg = "".join(["Error occurred in estimateDispersions:\n%s\n" % e,
                        "We will try with fitType=\"mean\"."])
            warnings.warn(msg)
            try:
                dds = deseq2.estimateDispersions_DESeqDataSet(dds, fitType="mean")
            except RuntimeError as e:
                msg = "".join(["Error occurred in estimateDispersions:\n%s\n" % e,
                            "We give up."])
                warnings.warn(msg)
                raise
    dds = deseq2.nbinomWaldTest(dds, betaPrior=deseq2_args["betaPrior"])
    with localconverter(ro.default_converter + pandas2ri.converter):
        res_rdf = as_df(deseq2.results(
            dds,
            contrast=contrast,
            addMLE=deseq2_args["addMLE"],
            independentFiltering=deseq2_args["independentFiltering"]))
        try:
            res = ro.conversion.rpy2py(res_rdf)
        except NotImplementedError as e:
            if str(e) == py2rpy_ndarray_issue:
                warnings.warn(str(e))
                warnings.warn("res_rdf:\n")
                warnings.warn(str(rdf))
                warnings.warn("\n")
            raise
    res.index = counts_data.index
    # return res, {cond : size_factors.loc[cond][0] for cond in cond_names}
    # Hopefully the order in size_factors is the same as in cond_names
    return res, dict(zip(cond_names, size_factors))
