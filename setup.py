# Copyright (C) 2020 Blaise Li
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from setuptools import setup, find_packages
#from Cython.Build import cythonize

name = "libdeseq"

# Adapted from Biopython
__version__ = "Undefined"
for line in open("%s/__init__.py" % name):
    if (line.startswith('__version__')):
        exec(line.strip())


setup(
    name=name,
    version=__version__,
    description="Interfacing the call to DESEq2 with python.",
    author="Blaise Li",
    author_email="blaise.li@normalesup.org",
    license="GNU GPLv3",
    packages=find_packages(),
    install_requires=[
        "pandas",
        "rpy2"]
    )
    #ext_modules = cythonize("libsmallrna/libsmallrna.pyx"),
    #install_requires=["cytoolz"],
    #zip_safe=False
